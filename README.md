## 500 led Christmas tree simulator

Matt Parker's Christmas tree in your browser. Now you can run unreviewed code as well!

This project is based on the work of Matt Parker, described in the video [My 500-LED xmas tree got into Harvard](https://www.youtube.com/watch?v=WuMRJf6B5Q4),
and on the [Computational Xmas Tree assignment](https://github.com/GSD6338/XmasTree) GitHub repo.

Please, watch the video, since Matt Parker is a significantly better explainer (and english-speaker) than me.

The assignment repository contains tools to simulate Christmas tree animations, but it requires additional software,
which I am frankly too lazy to get into. Therefore, I tried to create an entirely browser-based digital tree,
that allows you to preview your animations directly.

### I want to try it, but I don't have my own animation
Good news! You can try one of the [example files](https://github.com/GSD6338/XmasTree/tree/main/misc) provided in the XmasTree repository!

### I have a different LED configuration
Currently, the project uses Matt Parker's tree configuration.
If you really want to simulate a different tree, get in touch,
and I can try to implement a way to run the animations on custom trees. (It should be fairly simple, I just didn't get to it yet.)

### Will this steal your data?
I promise I will not steal your animations or data.
In fact, I do not collect any data (you can fact-check me by code-reviewing this repository).
The entire CSV file parsing and simulation is performed directly in your browser and
no data is sent to the server.

### Will my computer catch on fire?
Hopefully not. But there is only one way to find out.

I didn't perform virtually any optimizations of the code,
but my higher-end-ish laptop seems to handle it pretty well.

### It does not work
Please get in touch (see below) and we can try to help to resolve the issue.
Because of time constraints, I am unfortunately not ready to support older browser versions or mobile browsers.

### Get in touch
If you have any questions or ideas, feel free to get in touch - 
either create an issue in this repository, or you may email me at [tree@jenda.dev](tree@jenda.dev).
