import React, {useCallback, useEffect, useState} from "react";
import {
    Button,
    FormControl,
    FormLabel,
    NumberDecrementStepper,
    NumberIncrementStepper,
    NumberInput,
    NumberInputField,
    NumberInputStepper,
    Stack,
    UseCounterProps
} from "@chakra-ui/react";
import {useVisualizerConfig, VisualizerConfig} from "../../utils/VisualizerConfig";
import FloatingBox from "../FloatingBox";
import FileInput from "../FileInput";
import {useTranslation} from "next-i18next";

interface Props {
    setConfig: { (config: VisualizerConfig): void },
    isLoading?: boolean
}

const ConfigPanel = ({setConfig, isLoading = false}: Props) => {
    const {t} = useTranslation();
    const config = useVisualizerConfig();
    const [isProcessingFile, setIsProcessingFile] = useState<boolean>(false);

    const [localConfig, setLocalConfig] = useState<VisualizerConfig>(config);

    useEffect(() => {
        setLocalConfig({...config});
    }, [config]);

    const applyConfig = useCallback(() => {
        setConfig({...localConfig});
    }, [setConfig, localConfig]);

    const onAnimationSpeedChange: UseCounterProps['onChange'] = useCallback((_, numeric) => {
        setLocalConfig({...localConfig, animationSpeed: numeric});
    }, [localConfig]);

    const onFileChange = useCallback((file: File | null) => {
        if (file) {
            setIsProcessingFile(true);
            const reader = new FileReader();
            reader.addEventListener('load', (event) => {
                const dataUri = event.target?.result;
                if (dataUri) {
                    // @ts-ignore
                    setLocalConfig({...localConfig, animationString: dataUri});
                }
                setIsProcessingFile(false);
            });
            // @ts-ignore
            reader.readAsText(file);
        } else {
            setLocalConfig({...localConfig, animationString: null});
        }
    }, [localConfig]);

    return (
        <FloatingBox>
            <FormControl>
                <FormLabel htmlFor='interval'>{t("configPanel.interval.label")}</FormLabel>
                <NumberInput max={50000} min={1} value={localConfig.animationSpeed} onChange={onAnimationSpeedChange}>
                    <NumberInputField id='interval'/>
                    <NumberInputStepper>
                        <NumberIncrementStepper/>
                        <NumberDecrementStepper/>
                    </NumberInputStepper>
                </NumberInput>
            </FormControl>

            <FormControl mt={4}>
                <FormLabel>{t("configPanel.animation.label")}</FormLabel>
                <FileInput accept={"text/csv"} onChange={onFileChange}>
                    {t("configPanel.animation.fileButton")}
                </FileInput>
            </FormControl>

            <Stack direction={"row"} justifyContent={"end"} mt={4}>
                <Button isLoading={isLoading || isProcessingFile} onClick={applyConfig}>{t("configPanel.applyChanges")}</Button>
            </Stack>
        </FloatingBox>
    )
}

export default ConfigPanel;