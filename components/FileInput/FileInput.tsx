import {ChangeEvent, MouseEventHandler, ReactNode, useCallback, useRef, useState} from 'react'
import {Box, Button, Icon, InputGroup, Stack} from '@chakra-ui/react'
import {IoCloseCircleOutline} from "react-icons/io5";

type FileUploadProps = {
    accept?: string
    multiple?: boolean
    children?: ReactNode,
    onChange?: { (file: File | null): void }
}

const FileInput = ({accept, multiple, children, onChange}: FileUploadProps) => {
    const inputRef = useRef<HTMLInputElement | null>(null);
    const [uploadedFileName, setUploadedFileName] = useState<string | null>(null);

    const handleClick = () => inputRef.current?.click()

    const onInputChange = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        const [file] = event.target.files ?? [];
        setUploadedFileName(file?.name ?? "");
        if (onChange) {
            onChange(file);
        }
    }, [onChange]);

    const onClear: MouseEventHandler<HTMLButtonElement> = useCallback((e) => {
        e.stopPropagation();

        if (inputRef.current) {
            inputRef.current.value = "";
        }
        setUploadedFileName("");

        if (onChange) {
            onChange(null);
        }
    }, [onChange]);

    return (
        <InputGroup onClick={handleClick}>
            <input
                type={'file'}
                multiple={multiple || false}
                hidden
                accept={accept}
                ref={inputRef}
                onChange={onInputChange}
            />
            <Stack direction={"row"} alignItems={"center"} width={"100%"}>
                <Button>
                    {uploadedFileName ? uploadedFileName : children}
                </Button>
                <Box flex={1}/>
                <Box>
                    {uploadedFileName ? (
                        <Button onClick={onClear} variant={"ghost"} p={0}>
                            <Icon as={IoCloseCircleOutline} w={6} h={6}/>
                        </Button>
                    ) : null}
                </Box>
            </Stack>
        </InputGroup>
    )
}

export default FileInput;