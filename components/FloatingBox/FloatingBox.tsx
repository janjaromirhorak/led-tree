import React, {PropsWithChildren} from "react";
import {Box, useStyleConfig} from '@chakra-ui/react'

const FloatingBox = ({children}: PropsWithChildren<{}>) => {
    const styles = useStyleConfig('FloatingBox', {})
    return <Box __css={styles}>{children}</Box>
}

export default FloatingBox;