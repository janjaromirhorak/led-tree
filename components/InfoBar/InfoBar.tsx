import React from "react";
import {Box, Button, Icon, useStyleConfig} from "@chakra-ui/react";
import {AiFillGitlab} from "react-icons/ai";

const InfoBar = () => {
    const styles = useStyleConfig('InfoBar', {})

    return (
        <Box __css={styles}>
            <a href={"https://gitlab.com/janjaromirhorak/led-tree"} target={"_blank"} rel={"noopener noreferrer"}>
                <Button p={0}>
                    <Icon as={AiFillGitlab} w={6} h={6}/>
                </Button>
            </a>
        </Box>
    )
}

export default InfoBar;