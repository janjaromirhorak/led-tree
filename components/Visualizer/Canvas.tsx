import React, {PropsWithChildren} from "react";
import {Canvas as FiberCanvas} from "@react-three/fiber";
import {useStyleConfig} from "@chakra-ui/react";

const Canvas = ({children}: PropsWithChildren<{}>) => {
    const styles = useStyleConfig('Canvas', {})

    return (
        // @ts-ignore
        <FiberCanvas style={styles}>
            {children}
        </FiberCanvas>
    )
};

export default Canvas;