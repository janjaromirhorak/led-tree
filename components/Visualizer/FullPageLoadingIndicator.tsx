import React from "react";
import {Spinner, Stack, useStyleConfig} from '@chakra-ui/react'

const FullPageLoadingIndicator = () => {
    const styles = useStyleConfig('FullPageLoadingIndicator', {})
    return (
        <Stack __css={styles} direction={"row"} justifyContent={"center"} alignItems={"center"}>
            <Spinner size={"xl"} speed={"2s"}/>
        </Stack>
    )
}

export default FullPageLoadingIndicator;