import React, {useEffect, useMemo, useRef} from "react";
import * as THREE from "three";
import {useFrame} from "@react-three/fiber";
import {Sphere} from "@react-three/drei";
import LightData from "./LightData";
import {VisualizerConfig} from "../../utils/VisualizerConfig";

interface Props {
    lightData: LightData,
    config: VisualizerConfig
}

const Light = ({lightData, config}: Props) => {
    const lightMaterialRef = useRef<THREE.MeshBasicMaterial>(null!);
    let lastFrame = useRef<number>(0);
    let currentAnimationInterval = useRef<number>(config.animationSpeed);

    useEffect(() => {
        currentAnimationInterval.current = config?.animationSpeed;
    }, [config]);

    const frameCount = useMemo(() => {
        return lightData.colors.length / 3
    }, [lightData.colors.length]);

    const initialColor: string = useMemo(() => {
        let r = 0;
        let g = 0;
        let b = 0;

        if (lightData.colors.length >= 3) {
            r = lightData.colors[0];
            g = lightData.colors[1];
            b = lightData.colors[2];
        }

        return `rgb(${r},${g},${b})`;
    }, [lightData.colors]);

    useFrame(({ clock }) => {
        const currentFrame = Math.round((clock.getElapsedTime() * 1000) / currentAnimationInterval.current) % frameCount;
        if (currentFrame !== lastFrame.current) {
            const r = lightData.colors[currentFrame * 3];
            const g = lightData.colors[currentFrame * 3 + 1];
            const b = lightData.colors[currentFrame * 3 + 2];
            lightMaterialRef.current?.setValues({color: `rgb(${r},${g},${b})`});
            lastFrame.current = currentFrame;
        }
    })

    const fixedPosition: [number, number, number] = useMemo(() => {
        const [x, y, z] = lightData.position;
        return [x, z, y];
    }, [lightData.position]);

    return (
        <Sphere
            position={fixedPosition}
            scale={0.01}
        >
            <meshBasicMaterial
                ref={lightMaterialRef}
                attach="material"
                color={initialColor}
            />
        </Sphere>
    );
}

export default Light;