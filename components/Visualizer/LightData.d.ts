import LightPosition from "./LightPosition";

interface LightData {
    position: LightPosition,
    colors: Float32Array
}

export default LightData;