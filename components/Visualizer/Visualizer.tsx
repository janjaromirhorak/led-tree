import React, {useCallback, useEffect, useState} from 'react'
import LightData from "./LightData";
import {Center, OrbitControls} from "@react-three/drei";
import Light from "./Light";
import {useVisualizerConfig} from "../../utils/VisualizerConfig";
import blankAnimation from "./blankAnimation";
import {useToast} from '@chakra-ui/react'
import {useTranslation} from "next-i18next";
import FullPageLoadingIndicator from "./FullPageLoadingIndicator";
import Canvas from "./Canvas";
import {parse} from "csv";

const errorToastDuration = 10000;

const Visualizer = () => {
    const {t} = useTranslation();
    const config = useVisualizerConfig();
    const toast = useToast();
    const [lightsData, setLightsData] = useState<LightData[] | undefined>(undefined);

    const createBlankLightsData = useCallback(async (): Promise<LightData[]> => {
        const positionResponse = await fetch("/coords_2021.csv");
        const positionData = await positionResponse.text();

        const parsedData = await new Promise<string[][]>((resolve, reject) => {
            parse(positionData, {
                delimiter: ","
            }, (err, records) => {
                if (err) {
                    reject(err);
                }
                resolve(records);
            })
        })

        // @ts-ignore
        return parsedData
            .map(position => {
                return {
                    position: position.map(str => Number(str)),
                    colors: blankAnimation
                }
            })
            ;
    }, []);

    const applyAnimationFromString = useCallback(async (lightsData: LightData[], animationString: string): Promise<LightData[]> => {
        let animationData = await new Promise<number[][]>((resolve, reject) => {
            parse(animationString, {
                delimiter: ","
            }, (err, records: string[][]) => {
                if (err) {
                    toast({
                        title: t("visualizer.errors.invalidColorData.title"),
                        description: err.message,
                        status: 'error',
                        duration: errorToastDuration,
                        isClosable: true,
                    });
                    reject(err);
                    return;
                }
                if (!records) {
                    toast({
                        title: t("visualizer.errors.invalidColorData.title"),
                        description: t("visualizer.errors.invalidColorData.message"),
                        status: 'error',
                        duration: errorToastDuration,
                        isClosable: true,
                    });
                    reject(err);
                    return;
                }
                resolve(
                    records
                        // skip the first line
                        .slice(1)
                        // convert everything to numbers
                        .map(row => {
                            return row.map(item => Number(item))
                        })
                );
            })
        });

        for (const item of lightsData) {
            item.colors = new Float32Array(animationData.length * 3);
        }

        let frameIndex = 0;
        for (const row of animationData) {
            // skip the first column
            let frameColorData = row.slice(1);

            if (frameColorData.length) {
                if (frameColorData.length / 3 == lightsData.length) {
                    let lightIndex = 0;
                    while (frameColorData.length >= 3) {
                        const colors = frameColorData.splice(0, 3);

                        if (colors.some((number) => {
                            return number === undefined || isNaN(number) || number < 0 || number > 255
                        })) {
                            console.log(colors);
                            toast({
                                title: t("visualizer.errors.invalidColorData.title"),
                                description: t("visualizer.errors.invalidColorData.message"),
                                status: 'error',
                                duration: errorToastDuration,
                                isClosable: true,
                            });
                            throw Error();
                        }

                        if (lightsData[lightIndex]) {
                            lightsData[lightIndex].colors.set(colors, frameIndex * 3);
                        }
                        lightIndex++;
                    }
                } else {
                    toast({
                        title: t("visualizer.errors.invalidColorData.title"),
                        description: t("visualizer.errors.invalidColorData.message"),
                        status: 'error',
                        duration: errorToastDuration,
                        isClosable: true
                    });
                    throw Error();
                }
            }
            frameIndex++;
        }

        return lightsData;
    }, [t, toast]);

    const loadLights = useCallback(async () => {
        if (config.animationString) {
            try {
                setLightsData(await applyAnimationFromString(await createBlankLightsData(), config.animationString));
                return;
            } catch (e) {
                console.error(e);
            }
        }
        setLightsData(await createBlankLightsData());
    }, [config.animationString, applyAnimationFromString, createBlankLightsData]);

    useEffect(() => {
        loadLights().then();
    }, [loadLights]);

    if (!lightsData) {
        return <FullPageLoadingIndicator/>;
    }

    return (
        <Canvas>
            <OrbitControls enablePan={false} enableZoom={true} enableRotate={true}/>
            <Center>
                {lightsData.map((lightData, index) => {
                    return (
                        <Light key={index}
                               lightData={lightData}
                               config={config}
                        />
                    )
                })}
            </Center>
        </Canvas>
    );
}

export default Visualizer;