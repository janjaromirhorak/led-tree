import React from "react";
import { appWithTranslation } from 'next-i18next';

import {AppProps} from 'next/app'
import {ChakraProvider} from "@chakra-ui/react"

import theme from "../theme";

function App({Component, pageProps}: AppProps) {
  return (
      <ChakraProvider theme={theme}>
          <Component {...pageProps} />
      </ChakraProvider>
  )
}

export default appWithTranslation(App);
