import React from "react";
import Document, {DocumentContext, Html, Head, Main, NextScript} from 'next/document'
import {ColorModeScript} from "@chakra-ui/react"

import theme from "../theme"

class MyDocument extends Document {
    static async getInitialProps(ctx: DocumentContext) {
        return await Document.getInitialProps(ctx)
    }

    render() {
        return (
            <Html lang="cs">
                <Head/>
                <body>
                    <ColorModeScript initialColorMode={theme.config.initialColorMode}/>
                    <Main/>
                    <NextScript/>
                </body>
            </Html>
        )
    }
}

export default MyDocument
