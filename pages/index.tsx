import type {GetStaticProps, NextPage} from 'next'
import Visualizer from "../components/Visualizer";
import {useState} from "react";
import {defaultVisualizerConfig, VisualizerConfig, VisualizerContextProvider} from "../utils/VisualizerConfig";
import ConfigPanel from "../components/ConfigPanel";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import InfoBar from "../components/InfoBar";

const Home: NextPage = () => {
    const [config, setConfig] = useState<VisualizerConfig>(defaultVisualizerConfig);
    return (
        <VisualizerContextProvider value={config}>
            <ConfigPanel setConfig={setConfig}/>
            <InfoBar/>
            <Visualizer/>
        </VisualizerContextProvider>
    )
}

export const getStaticProps: GetStaticProps = async ({locale}) => {
    return {
        props: {
            // @ts-ignore
            ...(await serverSideTranslations(locale, ['common'])),
        }
    }
};

export default Home
