const FullPageLoadingIndicator = {
    baseStyle: {
        position: "absolute",
        left: "0",
        top: "0",
        zIndex: 10,
        background: "black",
        height: "100vh",
        width: "100vw"
    },
}

export default FullPageLoadingIndicator;