const FloatingBox = {
    baseStyle: {
        position: 'relative',
        zIndex: 100,
        borderRadius: 'base',
        background: 'gray.800',
        maxWidth: '20rem',
        left: '5',
        top: '5',
        padding: '5'
    },
}

export default FloatingBox;