const InfoBar = {
    baseStyle: {
        position: 'fixed',
        zIndex: 100,
        borderRadius: 'base',
        background: 'gray.800',
        maxWidth: '20rem',
        right: '5',
        bottom: '5'
    },
}

export default InfoBar;