import {extendTheme, ThemeConfig} from "@chakra-ui/react"
import FloatingBox from "./FloatingBox";
import FullPageLoadingIndicator from "./FullPageLoadingIndicator";
import Canvas from "./Canvas";
import InfoBar from "./InfoBar";

const config: ThemeConfig = {
    initialColorMode: "dark",
    useSystemColorMode: false
};

const extensions: any = {
    components: {
        Canvas,
        FloatingBox,
        FullPageLoadingIndicator,
        InfoBar
    }
};

const theme = extendTheme(extensions, {config});

export default theme;