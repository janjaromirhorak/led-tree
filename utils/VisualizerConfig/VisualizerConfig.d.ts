interface VisualizerConfig {
    animationSpeed: number;
    animationString: string | null;
}

export default VisualizerConfig;