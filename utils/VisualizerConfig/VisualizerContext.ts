import React from "react";
import VisualizerConfig from "./VisualizerConfig";
import defaultVisualizerConfig from "./defaultVisualizerConfig";

const VisualizerContext = React.createContext<VisualizerConfig>(defaultVisualizerConfig)

export default VisualizerContext;