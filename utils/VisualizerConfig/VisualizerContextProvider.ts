import {VisualizerContext} from "./index";

const VisualizerContextProvider = VisualizerContext.Provider;

export default VisualizerContextProvider;