import VisualizerConfig from "./VisualizerConfig";

const defaultVisualizerConfig: VisualizerConfig = {
    animationSpeed: 10,
    animationString: null
}

export default defaultVisualizerConfig;