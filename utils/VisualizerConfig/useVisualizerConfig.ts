import {useContext} from "react";
import VisualizerConfig from "./VisualizerConfig";
import {VisualizerContext} from "./index";

const useVisualizerConfig = () => {
    return useContext<VisualizerConfig>(VisualizerContext);
}

export default useVisualizerConfig;